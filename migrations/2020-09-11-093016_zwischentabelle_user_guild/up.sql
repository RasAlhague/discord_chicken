CREATE TABLE IF NOT EXISTS user_birthdays (
    user_id BIGINT UNSIGNED NOT NULL,
    guild_id BIGINT UNSIGNED NOT NULL,
    birthdate TIMESTAMP,   
    PRIMARY KEY (user_id, guild_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (guild_id) REFERENCES guilds(guild_id)
);

INSERT INTO user_birthdays (user_id, guild_id, birthdate) 
SELECT user_id, guild_id, birthdate
FROM users;

ALTER TABLE users
DROP FOREIGN KEY users_ibfk_1;

ALTER TABLE users
DROP COLUMN guild_id;
ALTER TABLE users
DROP COLUMN birthdate;
ALTER TABLE users 
ADD COLUMN guild_id BIGINT UNSIGNED NOT NULL;
ALTER TABLE users
ADD COLUMN birthdate TIMESTAMP;

ALTER TABLE users
ADD FOREIGN KEY (guild_id) REFERENCES guilds(guild_id);

UPDATE users 
INNER JOIN user_birthdays ON (users.user_id = user_birthdays.user_id)
SET users.birthdate = user_birthdays.birthdate, users.guild_id = user_birthdays.guild_id;

DROP TABLE user_birthdays;
CREATE TABLE IF NOT EXISTS guilds (
    guild_id INT(11) UNSIGNED PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
    user_id INT(11) UNSIGNED PRIMARY KEY,
    birthdate TIMESTAMP,
    guild_id INT(11) UNSIGNED NOT NULL,
    FOREIGN KEY (guild_id) REFERENCES guilds(guild_id)
);

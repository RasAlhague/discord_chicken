DROP TABLE IF EXISTS birthday_reminders;

CREATE TABLE IF NOT EXISTS user_birthdays (
    user_id BIGINT UNSIGNED NOT NULL,
    guild_id BIGINT UNSIGNED NOT NULL,
    birthdate TIMESTAMP,   
    PRIMARY KEY (user_id, guild_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (guild_id) REFERENCES guilds(guild_id)
);
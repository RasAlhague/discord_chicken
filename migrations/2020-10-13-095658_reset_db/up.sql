DROP TABLE IF EXISTS user_birthdays;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS guilds;

CREATE TABLE IF NOT EXISTS guilds (
    guild_id BIGINT UNSIGNED PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS users (
    user_id BIGINT UNSIGNED PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS birthday_reminders (
    user_id BIGINT UNSIGNED NOT NULL,
    guild_id BIGINT UNSIGNED NOT NULL,
    birthdate TIMESTAMP NOT NULL,   
    active BOOLEAN NOT NULL,
    PRIMARY KEY (user_id, guild_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (guild_id) REFERENCES guilds(guild_id)
);
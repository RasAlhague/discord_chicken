CREATE TABLE IF NOT EXISTS block_patterns (
    name VARCHAR(50) PRIMARY KEY,
    pattern VARCHAR(500) NOT NULL,
    guild_id BIGINT UNSIGNED NOT NULL,
    FOREIGN KEY (guild_id) REFERENCES guilds(guild_id)
);
# discord_chicken 

A bot for discord. A lot of features are about chickens, but it will get usefull features too.

Commands are prefixed with an ```~```, like ```~help```

## Permissions needed
 - dont know yet xD

## Chicken related features 
 - [x] Calling nick a chicken on request
 - [ ] Posting chicken pictures on request
 - [ ] making mean comments about people
 - [ ] give chicken trivia
 - [ ] send youtube vid links about chickens

## Non chicken related features
 - [ ] remind when an new anime episode is out
 - [ ] congratulate someone on their birthday
 - [ ] send emojis
 - [ ] react on messages
 - [ ] tiktok message handling
 -  - [x] auto delete tiktok messages
 -  - [ ] enable disable tiktok messages
 -  - [ ] delete exisiting tiktok messages
 - [ ] nhentai features
 -  - [x] send random nhentai link
 -  - [x] register a channel for nhentai link posts
 -  - [ ] unregister a channel for nhentai link posts
 - [x] pseudo scottish translator
 - [ ] feature request related features
 -  - [x] sending feature requests
 -  - [ ] displaying all requests
 - [ ] pokemon info helper
 - [ ] survey feature

## Possible features
 - [ ] Padoru for jan and marc

## Current comands 
 - ```help```: displays the commands that are possible for the bot
 - ```chicken```: calls nick a chicken with a random sentence
 - ```chick```: calls nick a chicken
 - ```quit```: Owner only, quits the bot.
 - ```random_hentai```: Sends a random link from nhentai in the channel. Channel must be registeres
 - ```register_nhentai```: Registers an channel for nhentai link posting.
 - ```request_feature```: Starts a dialog fpr requesting features

# VERY IMPORTANT
*NICK IS A CHICKEN!!!*
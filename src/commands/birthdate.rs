use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    CommandResult,
    macros::command,
};
use crate::database::{
    utils::{
        establish_connection, 
        DbBool
    }, 
    models::{
        BirthdayReminder, 
        User, 
        Guild
    }
};
use tracing::error;
use chrono::{NaiveDate};

#[command]
#[description = "Saves the database of the given user"]
async fn birthday(ctx: &Context, msg: &Message) -> CommandResult {
    if msg.guild_id.is_none() {
        msg.channel_id.say(&ctx.http, "Could not get guild information. Sadly i cannot save the birthdate").await?;

        return Ok(());
    }
    
    let guild_id = *msg.guild_id.unwrap().as_u64();
    let user_id = *msg.author.id.as_u64();
    
    let user = User::new(user_id);
    let guild = Guild::new(guild_id);
    let birthday_reminder = BirthdayReminder::new(user_id, guild_id, NaiveDate::from_ymd(2000, 1, 1).and_hms(0, 0, 0), true);

    let connection = establish_connection();

    let guild_exist_result = guild.exists(&connection);

    if let DbBool::DbError(why) = guild_exist_result {
        error!("Could not check if guild exists, err: {:?}", why);

        let _ = msg.reply(ctx, "An problem occured while setting birthdate!").await;

        return Ok(());
    } else if let DbBool::False = guild_exist_result {
        let insert_result = guild.insert(&connection);

        if let Err(why) = insert_result {
            error!("Could not insert guild, err: {:?}", why);

            let _ = msg.reply(ctx, "An problem occured while setting birthdate!").await;

            return Ok(());
        }
    }

    let user_exist_result = user.exists(&connection);

    if let DbBool::DbError(why) = user_exist_result {
        error!("Could not check if user exists, err: {:?}", why);

        let _ = msg.reply(ctx, "An problem occured while setting birthdate!").await;

        return Ok(());
    } else if let DbBool::False = user_exist_result {
        let insert_result = user.insert(&connection);

        if let Err(why) = insert_result {
            error!("Could not insert user, err: {:?}", why);

            let _ = msg.reply(ctx, "An problem occured while setting birthdate!").await;

            return Ok(());
        }
    }

    let birthday_reminder_exist_result = birthday_reminder.exists(&connection);

    if let DbBool::DbError(why) = birthday_reminder_exist_result {
        error!("Could not check if birthday reminder exists, err: {:?}", why);

        let _ = msg.reply(ctx, "An problem occured while setting birthdate!").await;

        return Ok(());
    } else if let DbBool::False = birthday_reminder_exist_result {
        let insert_result = birthday_reminder.insert(&connection);

        if let Err(why) = insert_result {
            error!("Could not insert birthday reminder, err: {:?}", why);

            let _ = msg.reply(ctx, "An problem occured while setting birthdate!").await;

            return Ok(());
        }
        else {
            let _ = msg.reply(ctx, "Birthdate set!").await;
        }
    }
    else {
        let update_result = birthday_reminder.update(&connection); 

        if let Err(why) = update_result {
            error!("Could not update birthday reminder, err: {:?}", why);

            let _ = msg.reply(ctx, "An problem occured while setting birthdate!").await;

            return Ok(());
        }
        else {
            let _ = msg.reply(ctx, "Birthdate updated!").await;
        }
    }

    Ok(())
}



#[command]
#[description = "Only says nick is a chicken."]
async fn unbirthday(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id.say(&ctx.http, "Nick is a chicken!").await?;

    Ok(())
}
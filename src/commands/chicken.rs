use rand::Rng;
use serenity::framework::standard::{macros::command, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;

#[command]
#[description = "Only says nick is a chicken."]
async fn chick(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id.say(&ctx.http, "Nick is a chicken!").await?;

    Ok(())
}

#[command]
#[description = "Says a random nick is a chicken sentence."]
async fn chicken(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id
        .say(&ctx.http, &random_chicken_text())
        .await?;

    Ok(())
}

fn random_chicken_text() -> String {
    let mut chicken_sentences = Vec::new();
    chicken_sentences.push("Nick is a chicken!");
    chicken_sentences.push("Nick is a shota chicken!");
    chicken_sentences.push("Nick is a little chicken!");
    chicken_sentences.push("Do you know nick? He is the little chicken from next door.");
    chicken_sentences.push("Wanna go to Nic-Fil-A?");
    chicken_sentences.push("What came first? The chicken or the Nick?");
    chicken_sentences.push("Why did the Nick cross the road?");

    let mut rng = rand::thread_rng();

    let random_number = rng.gen_range(0..chicken_sentences.len());

    chicken_sentences[random_number].to_string()
}

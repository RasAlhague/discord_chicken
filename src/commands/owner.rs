use crate::ShardManagerContainer;
use serenity::framework::standard::{macros::command, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;

#[command]
#[owners_only]
async fn quit(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;

    if let Some(manager) = data.get::<ShardManagerContainer>() {
        manager.lock().await.shutdown_all().await;
    } else {
        msg.reply(ctx, "There was a problem getting the shard manager")
            .await?;

        return Ok(());
    }

    msg.reply(ctx, "Shutting down!").await?;

    Ok(())
}

#[command]
#[owners_only]
async fn log(ctx: &Context, msg: &Message) -> CommandResult {
    use tracing::info;
    use crate::LOGFILE_PATH;

    info!("Log file has been requested!");

    let files = vec![LOGFILE_PATH];

    msg.channel_id.send_files(&ctx.http, files, |m| {
        m.content("Logfile send!")
    }).await?;

    info!("Log file has been send!");

    Ok(())
}
use gitlab_api_rs::ApiClient;

use tracing::{error, info};
use serenity::framework::standard::{macros::command, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;
use std::env;
use std::time::Duration;

fn get_gitlab_token() -> String {
    env::var("PRIVATE_TOKEN").expect("Gitlab api private token expected.")
}

#[command]
#[description = "Starts a feature request dialogue with the bot."]
async fn request_feature(ctx: &Context, msg: &Message) -> CommandResult {
    let _ = msg
        .reply(
            ctx,
            "Please give me the titel of the new Feature. You have 5 minutes.",
        )
        .await;

    let titel = match &msg
        .author
        .await_reply(&ctx)
        .timeout(Duration::from_secs(3600))
        .await
    {
        Some(answer) => answer.content.clone(),
        None => {
            let _ = msg.reply(ctx, "No answer within 3600 seconds.").await;

            String::new()
        }
    };

    let _ = msg
        .reply(
            ctx,
            "Please give me a description of the new Feature. You have 5 minutes.",
        )
        .await;

    let description = match &msg
        .author
        .await_reply(&ctx)
        .timeout(Duration::from_secs(3600))
        .await
    {
        Some(answer) => answer.content.clone(),
        None => {
            let _ = msg.reply(ctx, "No answer within 3600 seconds.").await;

            String::new()
        }
    };

    let api_token = get_gitlab_token();

    let issue = ApiClient::new(&api_token)
        .issue_req()
        .new_issue(19984034, &titel)
        .description(&description)
        .label("feature request")
        .send_async()
        .await;

    match issue {
        Ok(i) => {
            info!("Feature request has been saved under \"{:?}\"", i.web_url);

            let _ = msg
                .reply(
                    ctx,
                    format!(
                        "Your feature request has been collected. You can find it here: {}",
                        i.web_url
                    ),
                )
                .await;
        }
        Err(why) => {
            error!("Could not save the feature request, err: {:?}", why);

            let _ = msg.reply(ctx, "Could not save the feature request, someone chickend out before fixing the bug!").await;
        }
    };

    Ok(())
}

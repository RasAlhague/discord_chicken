pub mod tiktok {
    pub fn contains_tiktok(msg: &str) -> bool {
        use regex::Regex;

        let re = Regex::new(r"http[s]?//[a-z]+\.tiktok\.[a-z]+/").expect("Invalid regex!");

        re.is_match(msg)
    }
}

pub mod blocking {
    use crate::database::models::{BlockPattern, Guild};
    use crate::database::utils::*;
    use serenity::framework::standard::{macros::command, CommandResult};
    use serenity::model::prelude::*;
    use serenity::prelude::*;
    use std::time::Duration;
    use tracing::{error, info};

    pub fn contains_regex(msg: &str, guild_id: u64) -> bool {
        use regex::Regex;

        let conn = establish_connection();

        if let Ok(block_patterns) = BlockPattern::get_by_guild_id(guild_id, &conn) {
            for pattern in block_patterns {
                let re = Regex::new(&pattern.pattern).expect("Invalid regex!");

                if re.is_match(msg) {
                    info!("Message matches pattern with name: {}", pattern.name);

                    return true;
                }
            }

            return false;
        }
        error!("An error occured during getting pattern from the database!");
        return false;
    }

    #[command]
    #[allowed_roles("cock")]
    #[description = "Sends an random hentai link from n hentai."]
    async fn add_pattern(ctx: &Context, msg: &Message) -> CommandResult {
        let guild_id = match msg.guild_id {
            Some(id) => *id.as_u64(),
            None => 0,
        };

        let guild = Guild::new(guild_id);
        let conn = establish_connection();

        let res = guild.exists(&conn);
        match res {
            DbBool::True => (),
            DbBool::False => {
                if let Err(why) = guild.insert(&conn) {
                    error!("Could not insert guild, error: {:?}", why);

                    return Ok(());
                }
            }
            DbBool::DbError(why) => {
                error!("Could not insert guild, error: {:?}", why);

                msg.channel_id
                    .say(&ctx.http, "An error occured while trying to find the guild!")
                    .await?;

                    return Ok(());  
            }
        }

        let _ = msg
            .reply(
                ctx,
                "Please give me the name of the new pattern. You have 5 minutes.",
            )
            .await;

        let pattern_name = match &msg
            .author
            .await_reply(&ctx)
            .timeout(Duration::from_secs(3600))
            .await
        {
            Some(answer) => answer.content.clone(),
            None => {
                let _ = msg.reply(ctx, "No answer within 3600 seconds.").await;

                String::new()
            }
        };

        let _ = msg
            .reply(ctx, "Please give me the pattern. You have 5 minutes.")
            .await;

        let pattern = match &msg
            .author
            .await_reply(&ctx)
            .timeout(Duration::from_secs(3600))
            .await
        {
            Some(answer) => answer.content.clone(),
            None => {
                let _ = msg.reply(ctx, "No answer within 3600 seconds.").await;

                String::new()
            }
        };

        let block_pattern = BlockPattern::new(&pattern_name, &pattern, guild_id);
        
        let res = block_pattern.exists(&conn);
        match res {
            DbBool::True => {
                msg.channel_id
                    .say(&ctx.http, "A pattern with this name already exists")
                    .await?;

                Ok(())
            }
            DbBool::False => {
                if let Err(why) = block_pattern.insert(&conn) {
                    error!("Could not insert blockpattern, error: {:?}", why);

                    return Ok(());
                }

                info!("Pattern with name {} was added", pattern_name);

                msg.channel_id
                    .say(&ctx.http, "A pattern was succesfully added!")
                    .await?;

                Ok(())
            }
            DbBool::DbError(why) => {
                error!("Could not insert blockpattern, error: {:?}", why);

                msg.channel_id
                    .say(&ctx.http, "An error occured while trying to find the pattern!")
                    .await?;

                Ok(())
            }
        }
    }

    #[command]
    #[allowed_roles("cock")]
    #[description = "Sends an random hentai link from n hentai."]
    async fn del_pattern(ctx: &Context, msg: &Message) -> CommandResult {
        let guild_id = match msg.guild_id {
            Some(id) => *id.as_u64(),
            None => 0,
        };

        let _ = msg
            .reply(
                ctx,
                "Please give me the name of the new pattern. You have 5 minutes.",
            )
            .await;

        let pattern_name = match &msg
            .author
            .await_reply(&ctx)
            .timeout(Duration::from_secs(3600))
            .await
        {
            Some(answer) => answer.content.clone(),
            None => {
                let _ = msg.reply(ctx, "No answer within 3600 seconds.").await;

                String::new()
            }
        };

        let block_pattern = BlockPattern::new(&pattern_name, "", guild_id);

        let conn = establish_connection();
        let res = block_pattern.exists(&conn);
        match res {
            DbBool::True => {
                if let Err(why) = block_pattern.delete(&conn) {
                    error!("Could not delete blockpattern, error: {:?}", why);

                    return Ok(());
                }

                msg.channel_id
                    .say(&ctx.http, "Pattern successfull deleted")
                    .await?;

                Ok(())
            }
            DbBool::False => {
                msg.channel_id
                    .say(&ctx.http, "Pattern was not found!")
                    .await?;

                Ok(())
            }
            DbBool::DbError(why) => {
                error!("Could not insert blockpattern, error: {:?}", why);

                msg.channel_id
                    .say(&ctx.http, "An error occured while trying to find the pattern!")
                    .await?;

                Ok(())
            }
        }
    }
}

pub mod nhentai {
    use serenity::framework::standard::{macros::command, CommandResult};
    use serenity::model::prelude::*;
    use serenity::prelude::*;
    use std::fs::{File, OpenOptions};
    use std::io::LineWriter;
    use std::io::{self, prelude::*, BufReader};
    use tracing::error;

    static CHANNELS_FILE: &str = "./nhentai.csv";

    #[command]
    #[description = "Sends an random hentai link from n hentai."]
    async fn random_hentai(ctx: &Context, msg: &Message) -> CommandResult {
        let nhentai_channel = match NhentaiChannel::from_message(msg) {
            Some(n) => n,
            None => {
                msg.channel_id
                    .say(&ctx.http, "Could not retrieve channel informations.")
                    .await?;

                return Ok(());
            }
        };

        let allowed_channels = match NhentaiChannel::load(CHANNELS_FILE).await {
            Ok(a) => a,
            Err(why) => {
                msg.channel_id
                    .say(
                        &ctx.http,
                        "Could not get registered channels. Nhentai link could not be posted!",
                    )
                    .await?;
                error!("Could not get registered channels, error: {:?}", why);

                return Ok(());
            }
        };

        let resolved_link = match get_resolved_nhentai_link().await {
            Ok(r) => r,
            Err(why) => {
                msg.channel_id
                    .say(
                        &ctx.http,
                        "Something went wrong getting a random hentai!. Instead you will get the random link.",
                    )
                    .await?;
                error!("Could not resolve nhentai link, error: {:?}", why);

                String::from("https://www.nhentai.net/random/")
            }
        };

        if allowed_channels.into_iter().any(|c| {
            c.guild_id == nhentai_channel.guild_id && c.channel_id == nhentai_channel.channel_id
        }) {
            msg.channel_id.say(&ctx.http, resolved_link).await?;
            msg.channel_id
                .say(
                    &ctx.http,
                    "Here is your requested random hentai! Please don't fap to hard!",
                )
                .await?;
        } else {
            msg.channel_id
                .say(
                    &ctx.http,
                    "This channel is not meant to be lewd! Please ask a cock to change it.",
                )
                .await?;
        }

        Ok(())
    }

    #[command]
    #[description = "Registers an channel for nhentai link usage."]
    #[allowed_roles("cock")]
    async fn register_nhentai(ctx: &Context, msg: &Message) -> CommandResult {
        let nhentai_channel = match NhentaiChannel::from_message(msg) {
            Some(n) => n,
            None => {
                msg.channel_id
                    .say(
                        &ctx.http,
                        "Could not get guild. Channel could not be registered.",
                    )
                    .await?;

                return Ok(());
            }
        };

        let channels = match NhentaiChannel::load(CHANNELS_FILE).await {
            Ok(c) => c,
            Err(why) => {
                msg.channel_id
                    .say(
                        &ctx.http,
                        "Could not get registered channels. Channel could not be registered.",
                    )
                    .await?;
                error!("Could not register channel, error: {:?}", why);

                return Ok(());
            }
        };

        if channels.iter().any(|x| {
            x.channel_id == nhentai_channel.channel_id && x.guild_id == nhentai_channel.guild_id
        }) {
            msg.channel_id
                .say(
                    &ctx.http,
                    "Channel already registered! Have fun mastubating!",
                )
                .await?;
        } else {
            if let Err(why) = nhentai_channel.save(CHANNELS_FILE).await {
                msg.channel_id
                    .say(
                        &ctx.http,
                        "Could not get save guild. Channel could not be registered.",
                    )
                    .await?;
                error!("Could not get registeres channels, error: {:?}", why);

                return Ok(());
            }
        }

        msg.channel_id
            .say(&ctx.http, "Channel registered! Have fun mastubating!")
            .await?;

        Ok(())
    }

    // muss nach dem merge noch zuende implementiert werden
    async fn get_resolved_nhentai_link() -> reqwest::Result<String> {
        let html = reqwest::get("https://www.nhentai.net/random/")
            .await?
            .text()
            .await?;

        let regex =
            regex::Regex::new(r"/login/\?next=/g/([0-9]+)/").expect("Regex could not be unwrap!");

        let captures = regex.captures(&html).unwrap();

        Ok(format!(
            "https://www.nhentai.net/g/{}/",
            captures.get(1).unwrap().as_str()
        ))
    }

    struct NhentaiChannel {
        channel_id: u64,
        guild_id: u64,
    }

    impl NhentaiChannel {
        fn new(channel_id: u64, guild_id: u64) -> NhentaiChannel {
            NhentaiChannel {
                channel_id,
                guild_id,
            }
        }

        async fn save(&self, file_path: &str) -> io::Result<()> {
            let file_options = OpenOptions::new().append(true).open(file_path)?;

            let mut file = LineWriter::new(file_options);

            file.write_all(self.as_csv_string().as_bytes())?;

            Ok(())
        }

        async fn load(file_path: &str) -> Result<Vec<NhentaiChannel>, String> {
            let mut channels = Vec::new();

            let file = match File::open(file_path) {
                Ok(f) => f,
                Err(why) => return Err(format!("Could not read nhentai csv, why: {}", why)),
            };
            let reader = BufReader::new(file);

            for line in reader.lines() {
                if let Ok(l) = line {
                    let channel = match NhentaiChannel::from_csv_str(&l) {
                        Ok(c) => c,
                        Err(why) => {
                            return Err(format!("Could not read nhentai csv, why: {}", why));
                        }
                    };

                    channels.push(channel);
                }
            }

            Ok(channels)
        }

        fn from_message(msg: &Message) -> Option<NhentaiChannel> {
            let channel_id = msg.channel_id;
            let guild_id = match msg.guild_id {
                Some(id) => id,
                None => return None,
            };

            Some(NhentaiChannel::new(
                *channel_id.as_u64(),
                *guild_id.as_u64(),
            ))
        }

        fn from_csv_str(line: &str) -> Result<NhentaiChannel, String> {
            let vec: Vec<&str> = line.split(",").collect();

            let channel_id = match vec[0].parse::<u64>() {
                Ok(c) => c,
                Err(why) => {
                    return Err(format!("Could not parse channel id, why: {}", why));
                }
            };
            let guild_id = match vec[1].parse::<u64>() {
                Ok(c) => c,
                Err(why) => {
                    return Err(format!("Could not parse guild id, why: {}", why));
                }
            };

            Ok(NhentaiChannel {
                channel_id: channel_id,
                guild_id: guild_id,
            })
        }

        fn as_csv_string(&self) -> String {
            format!("{},{}", self.channel_id, self.guild_id)
        }
    }
}

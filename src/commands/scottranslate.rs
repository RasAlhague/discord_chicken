use serde::{Deserialize, Serialize};
use serenity::framework::standard::{macros::command, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;
use std::time::Duration;

#[derive(Serialize, Deserialize, Debug)]
struct ScottifyRequestData {
    #[serde(rename(serialize = "SourceId"))]
    source_id: String,
    #[serde(rename(serialize = "RegionId"))]
    region_id: String,
    #[serde(rename(serialize = "InputString"))]
    input_string: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct ScottifyRepsonseData {
    #[serde(alias = "Translation")]
    translation: String,
    #[serde(alias = "AudioURLEnglish")]
    audio_url_english: Option<String>,
    #[serde(alias = "AudioURLScottish")]
    audio_url_scottish: Option<String>,
    #[serde(alias = "AudioIsEnabled")]
    audio_is_enabled: Option<String>,
}

#[command]
#[description = "Scottifies an Text"]
async fn scottify(ctx: &Context, msg: &Message) -> CommandResult {
    let private_channel = msg.author.create_dm_channel(&ctx).await?;

    let source_text = match get_text_for_scottifing(&private_channel, ctx).await {
        Some(t) => t,
        None => return Ok(()),
    };

    let send_data = ScottifyRequestData {
        source_id: String::from("11"),
        region_id: String::from("1007"),
        input_string: source_text,
    };

    let received_data = get_scottified_response(send_data).await?;

    let target_channel_name = match get_name_of_target_channel(&private_channel, ctx).await {
        Some(t) => t,
        None => return Ok(()),
    };

    let _ = send_scottified_response(
        msg,
        &private_channel,
        ctx,
        &target_channel_name,
        received_data,
    )
    .await;

    Ok(())
}

async fn send_scottified_response(
    msg: &Message,
    private_channel: &PrivateChannel,
    ctx: &Context,
    target_channel_name: &str,
    received_data: ScottifyRepsonseData,
) -> CommandResult {
    let guild_id = match msg.guild_id {
        Some(g_id) => *g_id.as_u64(),
        None => {
            let _ = private_channel
                .say(ctx, "Could not get guild from server")
                .await;

            return Ok(());
        }
    };

    let channels = ctx.http.get_channels(guild_id).await?;

    match channels.iter().find(|m| m.name == target_channel_name) {
        Some(channel) => {
            channel.say(&ctx, received_data.translation).await?;

            private_channel
                .say(&ctx, "Scottified message send.")
                .await?
        }
        None => {
            private_channel
                .say(&ctx, "Could not find the searched channel!")
                .await?
        }
    };

    Ok(())
}

async fn get_text_for_scottifing(
    private_channel: &PrivateChannel,
    ctx: &Context,
) -> Option<String> {
    let _ = private_channel
        .say(&ctx, "Please send the text to scottify!")
        .await;

    match &private_channel
        .recipient
        .await_reply(&ctx)
        .timeout(Duration::from_secs(3600))
        .await
    {
        Some(answer) => Some(answer.content.clone()),
        None => {
            let _ = private_channel
                .say(ctx, "No answer within 3600 seconds.")
                .await;

            return None;
        }
    }
}

async fn get_name_of_target_channel(
    private_channel: &PrivateChannel,
    ctx: &Context,
) -> Option<String> {
    let _ = private_channel
        .say(
            &ctx,
            "Please send the name of the channel were to scottify!",
        )
        .await;

    match &private_channel
        .recipient
        .await_reply(&ctx)
        .timeout(Duration::from_secs(3600))
        .await
    {
        Some(answer) => Some(answer.content.clone()),
        None => {
            let _ = private_channel
                .say(ctx, "No answer within 3600 seconds.")
                .await;

            return None;
        }
    }
}

async fn get_scottified_response(
    send_data: ScottifyRequestData,
) -> reqwest::Result<ScottifyRepsonseData> {
    let client = reqwest::Client::new();
    let resp = client
        .post("http://www.scotranslate.com/Translation/Translate")
        .json(&send_data)
        .send()
        .await?;

    Ok(resp.json::<ScottifyRepsonseData>().await?)
}

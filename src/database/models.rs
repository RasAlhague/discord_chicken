use chrono::NaiveDateTime;
use diesel::mysql::MysqlConnection;
use diesel::prelude::*;
use diesel::result::QueryResult;
use super::utils::DbBool;
use super::schema::guilds; 
use super::schema::users; 
use super::schema::birthday_reminders;
use super::schema::block_patterns;

#[derive(Queryable, Insertable)]
pub struct Guild {
    pub guild_id: u64,
}

impl Guild {
    pub fn new(id: u64) -> Guild {
        Guild {
            guild_id: id,
        }
    }

    pub fn exists(&self, conn: &MysqlConnection) -> DbBool<String> {        
        use super::schema::guilds::dsl::*;
    
        let result = guilds.filter(guild_id.eq(self.guild_id))
            .load::<Guild>(conn);
    
        match result {
            Ok(g) => {
                if g.len() == 1 {
                    DbBool::True
                }
                else {
                    DbBool::False
                }
            },
            Err(why) => {
                DbBool::DbError(format!("Could not load guilds {}:", why))
            }
        }
    }

    pub fn insert(&self, conn: &MysqlConnection) -> QueryResult<usize> {
        diesel::insert_into(guilds::table)
            .values(self)
            .execute(conn)
    }
}

#[derive(Queryable, Insertable)]
pub struct BirthdayReminder {
    pub user_id: u64,
    pub guild_id: u64,
    pub birthdate: NaiveDateTime,
    pub active: bool,
}

impl BirthdayReminder {
    pub fn new(user_id: u64, guild_id: u64, birthdate: NaiveDateTime, active: bool) -> BirthdayReminder {
        BirthdayReminder {
            user_id,
            guild_id,
            birthdate,
            active,
        }
    }

    pub fn insert(&self, conn: &MysqlConnection) -> QueryResult<usize> {
        diesel::insert_into(birthday_reminders::table)
            .values(self)
            .execute(conn)
    }

    pub fn exists(&self, conn: &MysqlConnection) -> DbBool<String> {        
        use super::schema::birthday_reminders::dsl::*;
    
        let result = birthday_reminders
            .filter(user_id.eq(self.user_id))
            .filter(guild_id.eq(self.guild_id))
            .load::<BirthdayReminder>(conn);
    
        match result {
            Ok(g) => {
                if g.len() == 1 {
                    DbBool::True
                }
                else {
                    DbBool::False
                }
            },
            Err(why) => {
                DbBool::DbError(format!("Could not load users: {}", why))
            }
        }
    }

    pub fn update(&self, conn: &MysqlConnection) -> QueryResult<usize> {
        use super::schema::birthday_reminders::dsl::{birthday_reminders, active, birthdate};

        diesel::update(birthday_reminders.find((self.user_id, self.guild_id)))
            .set((active.eq(self.active), birthdate.eq(self.birthdate)))
            .execute(conn)
    } 
}

#[derive(Queryable, Insertable)]
pub struct User {
    pub user_id: u64,
}

impl User {
    pub fn new(id: u64) -> User {
        User {
            user_id: id,
        }
    }

    pub fn insert(&self, conn: &MysqlConnection) -> QueryResult<usize> {
        diesel::insert_into(users::table)
            .values(self)
            .execute(conn)
    }

    pub fn exists(&self, conn: &MysqlConnection) -> DbBool<String> {        
        use super::schema::users::dsl::*;
    
        let result = users
            .filter(user_id.eq(self.user_id))
            .load::<User>(conn);
    
        match result {
            Ok(g) => {
                if g.len() == 1 {
                    DbBool::True
                }
                else {
                    DbBool::False
                }
            },
            Err(why) => {
                DbBool::DbError(format!("Could not load users: {}", why))
            }
        }
    }
}

#[derive(Queryable, Insertable)]
pub struct BlockPattern {
    pub name: String,
    pub pattern: String,
    pub guild_id: u64,
}

impl BlockPattern {
    pub fn new(name: &str, pattern: &str, guild_id: u64) -> BlockPattern {
        BlockPattern {
            name: String::from(name),
            pattern: String::from(pattern),
            guild_id,
        }
    }

    pub fn get_by_guild_id(id: u64, conn: &MysqlConnection) -> Result<Vec<BlockPattern>, String> {
        use super::schema::block_patterns::dsl::*;

        let results = block_patterns.filter(guild_id.eq(id))
            .load::<BlockPattern>(conn);

        let patterns = match results {
            Ok(g) => g,
            Err(why) => {
                return Err(format!("Could not load guilds by id: {}", why))
            }
        };

        Ok(patterns)
    }

    pub fn insert(&self, conn: &MysqlConnection) -> QueryResult<usize> {
        diesel::insert_into(block_patterns::table)
            .values(self)
            .execute(conn)
    }

    pub fn exists(&self, conn: &MysqlConnection) -> DbBool<String> {        
        use super::schema::block_patterns::dsl::*;
    
        let result = block_patterns
            .filter(name.eq(self.name.clone()))
            .filter(guild_id.eq(self.guild_id))
            .load::<BlockPattern>(conn);
    
        match result {
            Ok(g) => {
                if g.len() == 1 {
                    DbBool::True
                }
                else {
                    DbBool::False
                }
            },
            Err(why) => {
                DbBool::DbError(format!("Could not load users: {}", why))
            }
        }
    }

    pub fn delete(&self, conn: &MysqlConnection) -> Result<(), String> {
        use super::schema::block_patterns::dsl::*;

        if let Err(why) = diesel::delete(block_patterns.filter(name.eq(self.name.clone())).filter(guild_id.eq(self.guild_id))).execute(conn) {
            return Err(format!("Could not delete pattern: {}", why))
        }

        Ok(())
    }

    pub fn update(&self, conn: &MysqlConnection) -> QueryResult<usize> {
        use super::schema::block_patterns::dsl::{block_patterns, pattern};

        diesel::update(block_patterns.find(self.name.clone()))
            .set(pattern.eq(self.pattern.clone()))
            .execute(conn)
    } 
}
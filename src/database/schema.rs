table! {
    birthday_reminders (user_id, guild_id) {
        user_id -> Unsigned<Bigint>,
        guild_id -> Unsigned<Bigint>,
        birthdate -> Timestamp,
        active -> Bool,
    }
}

table! {
    block_patterns (name) {
        name -> Varchar,
        pattern -> Varchar,
        guild_id -> Unsigned<Bigint>,
    }
}

table! {
    guilds (guild_id) {
        guild_id -> Unsigned<Bigint>,
    }
}

table! {
    users (user_id) {
        user_id -> Unsigned<Bigint>,
    }
}

joinable!(birthday_reminders -> guilds (guild_id));
joinable!(birthday_reminders -> users (user_id));
joinable!(block_patterns -> guilds (guild_id));

allow_tables_to_appear_in_same_query!(
    birthday_reminders,
    block_patterns,
    guilds,
    users,
);

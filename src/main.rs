#[macro_use]
extern crate diesel;

mod commands;
mod database;

use commands::{
    chicken::*,
    features::*,
    links::{nhentai::*, tiktok::*, blocking::*},
    owner::*,
    scottranslate::*,
    birthdate::*
};

use serenity::{
    async_trait,
    client::bridge::gateway::ShardManager,
    framework::standard::{
        help_commands,
        macros::{group, help},
        Args, CommandGroup, CommandResult, HelpOptions, StandardFramework,
    },
    http::Http,
    model::{channel::Message, event::ResumedEvent, gateway::Ready, id::UserId},
    prelude::*,
};
use std::{collections::HashSet, env, sync::Arc};
use tracing::{error, info};

pub const LOGFILE_PATH: &'static str = "./log_files/mainlog.log";

struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: Ready) {
        info!("Connected as {}", ready.user.name);

        if let Some(shard) = ready.shard {
            info!(
                "{} is connected on shard {}/{}!",
                ready.user.name, shard[0], shard[1],
            );
        }
    }

    async fn resume(&self, _: Context, _: ResumedEvent) {
        info!("Resumed");
    }

    async fn message(&self, ctx: Context, msg: Message) {
        if contains_tiktok(&msg.content) {
            if let Err(why) = msg.delete(&ctx.http).await {
                error!("Could not delete tiktok link, error: {:?}", why);
            } else {
                info!("Deleted Tiktok link");
            }

            let answer = msg.channel_id.say(&ctx.http, "Please don't send a tiktok link or else i will have to call the one and only divine cock!").await;

            if let Err(why) = answer {
                error!("Could not send answer on tiktok, error: {:?}", why);
            }
        }

        let guild_id = match msg.guild_id {
            Some(id) => *id.as_u64(),
            None => 0,
        };

        if contains_regex(&msg.content, guild_id) {
            if let Err(why) = msg.delete(&ctx.http).await {
                error!("Could not delete message, error: {:?}", why);
            } else {
                info!("Deleted message");
            }

            let answer = msg.channel_id.say(&ctx.http, "Please don't send a this message or else i will have to call the one and only divine cock!").await;

            if let Err(why) = answer {
                error!("Could not send answer on message, error: {:?}", why);
            }
        }
    }
}

#[group]
#[commands(request_feature, scottify)]
struct General;

#[group]
#[commands(chick, chicken)]
struct ChickenRelated;

#[group]
#[commands(random_hentai, register_nhentai, add_pattern, del_pattern)]
struct LinkRelated;

#[group]
#[owners_only]
// Limit all commands to be guild-restricted.
#[only_in(guilds)]
#[commands(quit, log)]
struct Owner;

#[group]
#[commands(birthday, unbirthday)]
struct BirthdateRelated;

#[tokio::main]
async fn main() {
    // This will load the environment variables located at `./.env`, relative to
    // the CWD. See `./.env.example` for an example on how to structure this.
    kankyo::load(false).expect("Failed to load .env file");

    let (non_blocking, _guard) = tracing_appender::non_blocking(std::fs::File::create(LOGFILE_PATH).unwrap());

    if cfg!(debug_assertions) {
        tracing_subscriber::fmt::init();
    }
    else {
        tracing_subscriber::fmt()
            .with_writer(non_blocking)
            .init();
    }

    let prefix = get_prefix();
    let token = get_token();

    let http = Http::new_with_token(&token);

    // We will fetch your bot's owners and id
    let (owners, _bot_id) = match http.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);

            (owners, info.id)
        }
        Err(why) => panic!("Could not access application info: {:?}", why),
    };

    // Create the framework
    let framework = StandardFramework::new()
        .configure(|c| c.owners(owners).prefix(&prefix))
        .help(&MY_HELP)
        .group(&CHICKENRELATED_GROUP)
        .group(&LINKRELATED_GROUP)
        .group(&OWNER_GROUP)
        .group(&BIRTHDATERELATED_GROUP)
        .group(&GENERAL_GROUP);

    let mut client = Client::builder(&token)
        .framework(framework)
        .event_handler(Handler)
        .await
        .expect("Err creating client");

    {
        let mut data = client.data.write().await;
        data.insert::<ShardManagerContainer>(client.shard_manager.clone());
    }

    if let Err(why) = client.start().await {
        error!("Client error: {:?}", why);
    }
}

fn get_prefix() -> String {
    if cfg!(debug_assertions) {
        String::from("!")
    } else {
        String::from("~")
    }
}

fn get_token() -> String {
    if cfg!(debug_assertions) {
        env::var("DEBUG_DISCORD_TOKEN").expect("Expected a token in the environment")
    } else {
        env::var("DISCORD_TOKEN").expect("Expected a token in the environment")
    }
}

// The framework provides two built-in help commands for you to use.
// But you can also make your own customized help command that forwards
// to the behaviour of either of them.
#[help]
// This replaces the information that a user can pass
// a command-name as argument to gain specific information about it.
#[individual_command_tip = "Hello! こんにちは！Hola! Bonjour! 您好!\n\
If you want more information about a specific command, just pass the command as argument."]
// Some arguments require a `{}` in order to replace it with contextual information.
// In this case our `{}` refers to a command's name.
#[command_not_found_text = "Could not find: `{}`."]
// Define the maximum Levenshtein-distance between a searched command-name
// and commands. If the distance is lower than or equal the set distance,
// it will be displayed as a suggestion.
// Setting the distance to 0 will disable suggestions.
#[max_levenshtein_distance(3)]
// On another note, you can set up the help-menu-filter-behaviour.
// Here are all possible settings shown on all possible options.
// First case is if a user lacks permissions for a command, we can hide the command.
#[lacking_permissions = "Hide"]
// The last `enum`-variant is `Strike`, which ~~strikes~~ a command.
#[wrong_channel = "Strike"]
// Serenity will automatically analyse and generate a hint/tip explaining the possible
// cases of ~~strikethrough-commands~~, but only if
// `strikethrough_commands_tip_{dm, guild}` aren't specified.
// If you pass in a value, it will be displayed instead.
async fn my_help(
    context: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>,
) -> CommandResult {
    let _ = help_commands::with_embeds(context, msg, args, help_options, groups, owners).await;
    Ok(())
}
